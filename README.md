# Proton Pack

Arduino code for a ghost chaser costume

## Hardware

The proton pack kit is based on an ESP9266 microcontroller. Here's the parts list:

- Adafruit Feather HUZZAH with ESP8266 (with integrated battery charging circuit!)
- A small rechargeable LiPo battery
- Some WS2812B-based programmable RGB LED strip
- Adafruit I2S 3W Class D Amplifier Breakout - MAX98357A
- A 100 Ohm resistor to adjust the amplifier's, well, amplification
- A small (3 cm) 4 Ohm speaker
- A push buttons

## Software

We use Visual Studio Code for development, leveraging the PlatformIO extension for microcontroller-specific tasks.

## Resources 

Sounds were designed using the Cardinal modular synthesizer in Qtractor, post-processing is performed using Ardour.

## Operating System

All work is, of course, done on Debian Linux! :)
