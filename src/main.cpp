#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <ESP8266Audio.h>
#include <LittleFS.h>
#include <AudioFileSourceLittleFS.h>
#include <AudioGeneratorMP3.h>
#include <AudioGeneratorWAV.h>
#include <AudioOutputI2S.h>
#include <SimplexNoise.h>

// --- Definitions and declarations -----------------------------------


// --- Audio ---

#define I2S_DI_PIN 3    // RX
#define I2S_BCLK_PIN 15 // D8
#define I2S_LRC_PIN 2   // D4

#define AUDIO_GAIN 2

const char *BEAM_SOUND_FILENAME = "/beam.mp3";

AudioFileSource *beam_sound_file;
AudioGenerator *audio_player;
AudioOutput *audio_out;

// --- LED strip ---

#define LED_STRIP_PIN 13 // D7
#define TOTAL_LED_COUNT 40
#define LED_OFFSET 10

const int LED_COUNT = int(TOTAL_LED_COUNT / 2.0 + 0.5);

const int LED_DELAY_MS = 4;

const float OFF_COLOR[3] = {0, 0, 0};

const float NOISE_COLOR[3] = {128, 0, 0};
const float NOISE_PHASE_VELOCITY = -0.0005;
const float NOISE_LAMBDA = 0.1;

const float WAVE_COLOR[3] = {255, 16, 0};
const float WAVE_PHASE_VELOCITY = -16.0;
const float WAVE_LAMBDA = 1.0;

const float FLASH_COLOR[3] = {0, 128, 512};
const float FLASH_PROBABILITY = 0.05;
const float FLASH_DECAY_FACTOR = 0.9;

Adafruit_NeoPixel led_strip(LED_OFFSET + TOTAL_LED_COUNT, LED_STRIP_PIN, NEO_GRB + NEO_KHZ800);

float wave_pattern[TOTAL_LED_COUNT];
float noise_pattern[TOTAL_LED_COUNT];
float flash_pattern[TOTAL_LED_COUNT];
float led_colors[TOTAL_LED_COUNT][3];

SimplexNoise sn;

long last_led_update_timestamp = 0;

// --- Input ---

#define TRIGGER_BUTTON_PIN 0      // D3

#define BUTTON_DEBOUNDING_DELAY_MS 100

long last_trigger_button_pressed_timestamp = 0;
long last_trigger_button_released_timestamp = 0;

bool active = false;

// --- Helper functions -----------------------------------------------

void push_led_colors()
{
  for (int pixel = 0; pixel < LED_COUNT; pixel++)
  {
    int red;
    int green;
    int blue;
    if (active) 
    {
      red = min(led_colors[pixel][0], (float)255.0);
      green = min(led_colors[pixel][1], (float)255.0);
      blue = min(led_colors[pixel][2], (float)255.0);
    }
    else
    {
      red = 0;
      green = 0;
      blue = 0;
    }
    led_strip.setPixelColor(
        LED_OFFSET + pixel,
        static_cast<unsigned int>(red),
        static_cast<unsigned int>(green),
        static_cast<unsigned int>(blue)
    );
    led_strip.setPixelColor(
        LED_OFFSET + TOTAL_LED_COUNT - pixel - 1,
        static_cast<unsigned int>(red),
        static_cast<unsigned int>(green),
        static_cast<unsigned int>(blue)
    );
  }
  led_strip.show();
}

void set_led_color(int pixel, const float colors[3])
{
  for (int c = 0; c < 3; c++)
  {
    led_colors[pixel][c] = colors[c];
  }
}

void set_led_color(int pixel, const float red, const float green, const float blue)
{
  led_colors[pixel][0] = red;
  led_colors[pixel][1] = green;
  led_colors[pixel][2] = blue;
}

void initialize_led_colors()
{
  for (int pixel = 0; pixel < TOTAL_LED_COUNT; pixel++)
  {
    set_led_color(pixel, OFF_COLOR);
  }
  push_led_colors();
}

void apply_tint(float color[], const float tint[], const float strength)
{
  for (int c = 0; c < 3; c++)
  {
    color[c] = (1 - strength)* color[c] + strength * tint[c];
  }
}

void apply_led_pattern(float pattern[], const float color[])
{
  for (int pixel = 0; pixel < TOTAL_LED_COUNT; pixel++)
  {
    apply_tint(led_colors[pixel], color, pattern[pixel]);
  }
}

void update_noise_pattern(float pattern[])
{
  float phi = NOISE_PHASE_VELOCITY * millis();
  for (int pixel = 0; pixel < LED_COUNT; pixel++)
  {
    double value = 0.5 + sn.noise(NOISE_LAMBDA * pixel - phi, 0)/2.0;
    pattern[pixel] = value * value;
  }
}

void update_wave_pattern(float pattern[])
{
  float phi = WAVE_PHASE_VELOCITY * millis()/1000.0;
  for (int pixel = 0; pixel < LED_COUNT; pixel++)
  {
    float value = 0.5 + sin(WAVE_LAMBDA * pixel + phi)/2.0;
    pattern[pixel] = value * value;
  }
}

float random_float(float max)
{
  return random(10000.0 * max)/10000.0;
}

void update_flash_pattern(float pattern[])
{
  if (random_float(1.0) < FLASH_PROBABILITY) {
    pattern[random(TOTAL_LED_COUNT)] = 1.0;
  }
  for (int pixel = 0; pixel < LED_COUNT; pixel++)
  {
    float value = FLASH_DECAY_FACTOR * pattern[pixel];
    pattern[pixel] = value;
  }
}

void update_led_colors()
{
  initialize_led_colors();
  update_noise_pattern(noise_pattern);
  apply_led_pattern(noise_pattern, NOISE_COLOR);
  update_wave_pattern(wave_pattern);
  apply_led_pattern(wave_pattern, WAVE_COLOR);
  update_flash_pattern(flash_pattern);
  apply_led_pattern(flash_pattern, FLASH_COLOR);
}

// --- Input handlers -------------------------------------------------

ICACHE_RAM_ATTR void onTriggerButtonChanged()
{
  if (digitalRead(TRIGGER_BUTTON_PIN) == HIGH) {
    active = false;
  }
  else {
    active = true;
  }
}

// --- Setup ----------------------------------------------------------

void setup()
{

  // --- Misc ---

  delay(500);
  Serial.begin(115200);

  if (LittleFS.begin())
  {
    Serial.println("Filesystem successfully mounted.");
  }
  else
  {
    Serial.println("Error mounting filesystem!");
  }

  Serial.println("Files in root directory:");
  Dir root = LittleFS.openDir("/");
  while (root.next())
  {
    Serial.println(root.fileName());
  }
  Serial.println("---");

  // --- Audio ---

  audio_player = new AudioGeneratorMP3();
  audio_out = new AudioOutputI2S();
  audio_out->SetGain(AUDIO_GAIN);

  // --- LED strip ---

  led_strip.begin();

  initialize_led_colors();

  // --- Input ---

  pinMode(TRIGGER_BUTTON_PIN, INPUT_PULLUP);
  attachInterrupt(TRIGGER_BUTTON_PIN, onTriggerButtonChanged, CHANGE);

}

// --- Main loop ------------------------------------------------------

void loop()
{

  // --- Misc ---

  // --- Audio ---

  if (active)
  {
    if (!audio_player->isRunning())
    {
      AudioFileSource *audio_source = new AudioFileSourceLittleFS(BEAM_SOUND_FILENAME);
      audio_player->begin(audio_source, audio_out);
    }
  }
  else {
    if (audio_player->isRunning())
    {
      audio_player->stop();
    }
  }

  if (audio_player->isRunning())
  {
    if (!audio_player->loop())
    {
      audio_player->stop();
    }
  }

  // --- LED strip ---

  long now = millis();
  if (now - LED_DELAY_MS > last_led_update_timestamp)
  {
    last_led_update_timestamp = now;
    update_led_colors();
    push_led_colors();
  }
}
